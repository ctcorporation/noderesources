﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;

namespace NodeResources
{
    public class CustomValidateHandler
    {
        private static bool isValid;
        private static IList<string> myValList = new List<string>();

        public IList<string> ValidationList
        {
            get { return myValList; }
            set { myValList = value; }
        }

        public bool IsValid
        {
            get { return isValid; }
            set { isValid = value; }
        }

        public static void HandlerErrors(object sender, ValidationEventArgs e)
        {
            if (e.Severity == XmlSeverityType.Error)
            {
                isValid = false;
                myValList.Add(
                    string.Format(
                    Environment.NewLine + "Line: {0}, Position {1}: \"{2}\"",
                    e.Exception.LineNumber,
                    e.Exception.LinePosition,
                    e.Exception.Message));
            }

        }
    }
}
