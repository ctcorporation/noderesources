﻿using System;

namespace NodeResources
{
    public class LegAddress : ILegAddress
    {
        private string containerNo;
        private Guid addressID;
        private string legType;
        private string dateType;
        private string legDate;
        private string dropMode;
        private IPackingSet packSet;
        private decimal weight;
        private int legNo;


        public string ContainerNo
        {
            get { return containerNo; }
            set { containerNo = value; }
        }

        public Guid AddressId
        {
            get { return addressID; }
            set { addressID = value; }
        }
        public string LegType
        {
            get { return legType; }
            set { legType = value; }
        }
        public string DateType
        {
            get { return dateType; }
            set { dateType = value; }
        }
        public string LegDate
        {
            get { return legDate; }
            set { legDate = value; }

        }

        public string DropMode
        {
            get { return dropMode; }
            set { dropMode = value; }
        }

        public IPackingSet PackSet
        {
            get { return packSet; }
            set { packSet = value; }
        }

        public decimal Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        public int LegNo
        {
            get { return legNo; }
            set { legNo = value; }
        }

    }



}
