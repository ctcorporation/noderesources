﻿namespace NodeResources
{
    public interface IPackingSet
    {
        string PackType { get; set; }
        int Pcs { get; set; }
        decimal Volume { get; set; }
        decimal Weight { get; set; }
    }
}