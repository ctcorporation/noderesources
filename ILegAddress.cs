﻿using System;

namespace NodeResources
{
    public interface ILegAddress
    {
        Guid AddressId { get; set; }
        string ContainerNo { get; set; }
        string DateType { get; set; }
        string DropMode { get; set; }
        string LegDate { get; set; }
        int LegNo { get; set; }
        string LegType { get; set; }
        IPackingSet PackSet { get; set; }
        decimal Weight { get; set; }
    }
}