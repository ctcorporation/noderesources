﻿using Ionic.Zip;
using Microsoft.Win32.SafeHandles;
using NodeResources.Interfaces;
using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;

namespace NodeResources
{
	public class ArcOps : IDisposable, IArcOps
	{
		bool disposed = false;
		// Instantiate a SafeHandle instance.
		SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
		private int GetWeekNumberOfMonth(DateTime date)
		{
			date = date.Date;
			DateTime firstMonthDay = new DateTime(date.Year, date.Month, 1);
			DateTime firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
			if (firstMonthMonday > date)
			{
				firstMonthDay = firstMonthDay.AddMonths(-1);
				firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
			}
			return (date - firstMonthMonday).Days / 7 + 1;
		}


		public string ExtractFile(string archive, string fileName, string Location)
		{
			string result = string.Empty;
			try
			{
				var zipFile = ZipFile.Read(@archive);
				var arcFound = zipFile.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fileName)));
				zipFile.Dispose();
				if (arcFound)
				{
					using (ZipFile Zip1 = ZipFile.Read(archive))
					{
						foreach (ZipEntry e in Zip1.Where(x => x.FileName == Path.GetFileName(fileName)))
						{
							e.Extract(Location);
						}

						Zip1.Dispose();
					}
				}
				else
				{
					result = "Warning: Specified File not found in Archive.";
				}


			}
			catch (Exception ex)
			{
				string strEx = ex.GetType().Name;
				result = "Warning: " + strEx + "Exception found. Error is: " + ex.Message;
			}
			return result;

		}

		public string ArchiveFile(string arcPath, string fileName, string logPath)
		{
			string lo = logPath;
			string cp = string.Empty;
			DateTime ti = DateTime.Now;
			string er = string.Empty;
			string di = string.Empty;


			string archive = "";
			string archiveName = "Week-" + GetWeekNumberOfMonth(DateTime.Now).ToString() + string.Format("{0:MMMyyyy}", DateTime.Now);
			try
			{

				if (!File.Exists(Path.Combine(arcPath, archiveName + ".ZIP")))
				{
					using (ZipFile ZipNew = new ZipFile())
					{
						string newArchive = Path.Combine(arcPath, archiveName + ".ZIP");
						ZipNew.AddFile(fileName, "");
						ZipNew.Save(Path.Combine(newArchive));
						archive = newArchive;
						ZipNew.Dispose();

					}
				}
				else
				{
					using (ZipFile zipFile = new ZipFile(@Path.Combine(arcPath, archiveName + ".ZIP")))
					{
						if (!zipFile.ContainsEntry(fileName))
						{
							try
							{
								zipFile.AddFile(fileName, "");

							}
							catch (ArgumentException ex)
							{
								string exType = ex.GetType().Name;
								return "Exception:" + exType + ":" + ex.Message;
							}
						}
						else
						{
							zipFile.UpdateFile(fileName);
						}
						zipFile.Save();
						archive = Path.Combine(arcPath, archiveName + ".ZIP");
					}



					using (ZipFile Zip1 = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP")))
					{


						Zip1.Dispose();


					}
				}
				//   File.Delete(fileName);
			}
			catch (Exception ex)
			{
				er = ex.GetType().Name + ": " + ex.Message;
				return "Exception:" + er + ":" + ex.Message;
			}

			return Path.GetFileName(archive);
		}
		public string ArchiveFile(string arcPath, string fileName)
		{
			//string lo = Globals.AppLogPath;



			string archive = "";
			string archiveName = "Week-" + GetWeekNumberOfMonth(DateTime.Now).ToString() + string.Format("{0:MMMyyyy}", DateTime.Now);
			try
			{

				if (!File.Exists(Path.Combine(arcPath, archiveName + ".ZIP")))
				{
					using (ZipFile ZipNew = new ZipFile())
					{
						string newArchive = Path.Combine(arcPath, archiveName + ".ZIP");
						ZipNew.AddFile(fileName, "");
						ZipNew.Save(Path.Combine(newArchive));
						archive = newArchive;

					}
				}
				else
				{
					using (ZipFile zipFile = new ZipFile(@Path.Combine(arcPath, archiveName + ".ZIP")))
					{
						if (!zipFile.ContainsEntry(Path.GetFileName(fileName)))
						{
							try
							{
								zipFile.AddFile(fileName, "");
							}
							catch (Exception ex)
							{
								string exType = ex.GetType().Name;
								return "Exception:" + exType + ":" + ex.Message;
							}
						}
						else
						{
							zipFile.UpdateFile(fileName);
						}
						zipFile.Save();
						archive = Path.Combine(arcPath, archiveName + ".ZIP");
					};




				}
				//   File.Delete(fileName);
			}
			catch (Exception ex)
			{
				string exType = ex.GetType().Name;
				return "Exception:" + exType + ":" + ex.Message;
			}

			return Path.GetFileName(archive);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Protected implementation of Dispose pattern.
		protected virtual void Dispose(bool disposing)
		{
			if (disposed)
			{
				return;
			}

			if (disposing)
			{
				handle.Dispose();
				// Free any other managed objects here.
				//
			}

			disposed = true;
		}

	}
}
