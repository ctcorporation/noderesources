﻿namespace NodeResources
{
    public class MsgBody : IMsgBody
    {
        public string MsgFrom
        {
            get;
            set;
        }
        public string MsgSubject
        {
            get;
            set;
        }
        public string MsgText
        {
            get; set;

        }
        public string MsgStatus
        {
            get;
            set;
        }

        public string MsgTo
        {
            get;
            set;
        }
    }
}
