﻿using Microsoft.Win32.SafeHandles;
using OpenPop.Pop3;
using OpenPop.Pop3.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;

namespace NodeResources
{
    public class MailModule : IDisposable
    {
        #region members
        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);
        private bool mailSent = false;
        private string _errMsg;
        private string _alertsTo;
        private string _customReceive;
        private Pop3Client client = new Pop3Client();
        private bool _errorsFound = false;
        #endregion

        #region properties
        public string AlertsTo
        {
            get
            { return _alertsTo; }
            set
            { _alertsTo = value; }
        }

        public string CustomReceive
        {
            get
            { return _customReceive; }
            set
            { _customReceive = value; }
        }

        public bool ErrorsFound
        {
            get
            { return _errorsFound; }
            set
            { _errorsFound = value; }
        }

        public IMailServerSettings MailServerSettings { get; set; }

        public string ErrMsg
        {
            get
            { return _errMsg; }

            set
            { _errMsg = value; }
        }

        #endregion

        #region constructors
        public MailModule(IMailServerSettings _mailServer)
        {
            this.MailServerSettings = _mailServer;
        }


        public MailModule(IMailServerSettings serverSettings, string alertsTo, string customReceive)
        {
            MailServerSettings = serverSettings;
            AlertsTo = alertsTo;
            CustomReceive = customReceive;
        }

        #endregion

        #region methods

        private bool ConnectMail()
        {
            try
            {

                if (client.Connected)
                {
                    client.Disconnect();
                }
                client = new Pop3Client();
                while (!client.Connected)

                {
                    client.Connect(MailServerSettings.Server, MailServerSettings.Port, MailServerSettings.IsSecure);
                }
                if (client.Connected)
                {
                    client.Authenticate(MailServerSettings.UserName, MailServerSettings.Password);
                }

            }
            catch (ArgumentOutOfRangeException ex)
            {
                _errMsg += ex.GetType().Name + ": Please ensure the Port is correct or Timeout is correct.";
                _errorsFound = true;
            }
            catch (PopServerNotAvailableException ex)
            {
                _errMsg += ex.GetType().Name + ": POP Server did not send reply when connected.";
                _errorsFound = true;
            }
            catch (PopServerNotFoundException ex)
            {
                _errMsg += ex.GetType().Name + ": POP Server not found. Please check POP Server or Network settings.";
                _errorsFound = true;
            }
            return client.Connected;
        }
        public List<IMsgSummary> GetMail()
        {
            int messageCount = 0;
            string fromMail = "";
            string subject = "";
            List<IMsgSummary> allMessages = new List<IMsgSummary>();
            if (ConnectMail())
            {
                try
                {

                    messageCount = client.GetMessageCount();
                    if (messageCount > 0)
                    {

                        for (int i = messageCount; i > 0; i--)
                        {
                            if (client.GetMessage(i) != null)
                            {
                                OpenPop.Mime.Message msg = client.GetMessage(i);
                                MsgSummary newSumm = new MsgSummary();
                                try
                                {
                                    if (msg.Headers.From.MailAddress.Address != null && msg.Headers.From.MailAddress.Address != "")
                                    {
                                        fromMail = msg.Headers.From.MailAddress.Address;
                                    }
                                    if (msg.Headers.Subject != null && msg.Headers.Subject != "")
                                    {
                                        subject = msg.Headers.Subject;
                                    }
                                    List<IMsgAtt> att = new List<IMsgAtt>();

                                    foreach (OpenPop.Mime.MessagePart attach in msg.FindAllAttachments())
                                    {
                                        string file_name_attach = attach.FileName;
                                        FileInfo fi = new FileInfo(Path.Combine(CustomReceive, file_name_attach));
                                        att.Add(new MsgAtt() { AttFilename = fi });
                                        attach.Save(fi);
                                    }
                                    newSumm.MsgFile = att;
                                    newSumm.MsgFrom = fromMail;
                                    newSumm.MsgTo = msg.Headers.To[0].MailAddress.ToString();
                                    newSumm.MsgSubject = subject;
                                    newSumm.MsgStatus = "Ok";
                                    client.DeleteMessage(i);
                                }
                                catch (Exception exAtt)
                                {
                                    newSumm.MsgStatus = exAtt.Message;

                                }

                                finally
                                {
                                    allMessages.Add(newSumm);
                                }

                            }

                        }
                    }

                }

                catch (Exception ex)
                {
                    _errMsg += "Exception:" + ex.GetType().Name + ": " + ex.Message + Environment.NewLine;
                    _errorsFound = true;
                }
            }

            client.Disconnect();
            return allMessages;

        }

        public IEnumerable<IMsgBody> GetMailBody()
        {
            int messageCount = 0;
            string fromMail = "";
            string subject = "";
            List<IMsgBody> allMessages = new List<IMsgBody>();
            try
            {
                client = new Pop3Client();
                client.Connect(MailServerSettings.Server, MailServerSettings.Port, MailServerSettings.IsSecure);
                if (client.Connected)
                {
                    client.Authenticate(MailServerSettings.UserName, MailServerSettings.Password);
                }
                messageCount = client.GetMessageCount();
                if (messageCount > 0)
                {
                    for (int i = messageCount; i > 0; i--)
                    {
                        if (client.GetMessage(i) != null)
                        {
                            OpenPop.Mime.Message msg = client.GetMessage(i);
                            IMsgBody newSumm = new MsgBody();
                            try
                            {
                                if (msg.Headers.From.MailAddress.Address != null && msg.Headers.From.MailAddress.Address != "")
                                {
                                    fromMail = msg.Headers.From.MailAddress.Address;
                                }
                                if (msg.Headers.Subject != null && msg.Headers.Subject != "")
                                {
                                    subject = msg.Headers.Subject;
                                }
                                OpenPop.Mime.MessagePart plainText = msg.FindFirstPlainTextVersion();
                                if (plainText != null)
                                {
                                    // We found some plaintext!
                                    newSumm.MsgText = plainText.GetBodyAsText();
                                }
                                else
                                {
                                    // Might include a part holding html instead
                                    OpenPop.Mime.MessagePart html = msg.FindFirstHtmlVersion();
                                    if (html != null)
                                    {
                                        // We found some html!
                                        newSumm.MsgText = html.GetBodyAsText();
                                    }
                                }

                                newSumm.MsgFrom = fromMail;
                                newSumm.MsgTo = msg.Headers.To[0].MailAddress.ToString();
                                newSumm.MsgSubject = subject;
                                newSumm.MsgStatus = "Ok";
                                client.DeleteMessage(i);
                            }
                            catch (Exception exAtt)
                            {
                                newSumm.MsgStatus = exAtt.Message;

                            }
                            finally
                            {
                                allMessages.Add(newSumm);
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                _errMsg += "Exception:" + ex.GetType().Name + ": " + ex.Message + Environment.NewLine;
                _errorsFound = true;
            }
            client.Disconnect();
            return allMessages;
        }

        public string CheckMail()
        {
            string mailStatus = "";
            //MailClient client = new MailClient("Checking");
            //MailServer server = new MailServer(hostname,username,password,ServerProtocol.Pop3);
            //server.SSLConnection = useSsl;
            //server.Port = port;
            client = new Pop3Client();
            try
            {
                //client.Connect(server);

                client.Connect(MailServerSettings.Server, MailServerSettings.Port, MailServerSettings.IsSecure);
                if (client.Connected)
                {
                    mailStatus += "Server " + MailServerSettings.Server + ":" + MailServerSettings.Port.ToString() + ", Connected. Ok." + Environment.NewLine;
                    try
                    {

                        client.Authenticate(MailServerSettings.UserName, MailServerSettings.Password);
                        mailStatus += "Username and Password details are Correct. Mail Connection Connected. Ok." + Environment.NewLine;
                    }
                    catch (Exception)
                    {
                        mailStatus += "Username and/or Password are incorrect. Please confirm the credentials" + Environment.NewLine;
                    }


                }
                else
                {
                    mailStatus += "Warning. Server " + MailServerSettings.Server + ":" + MailServerSettings.Port.ToString() + ", Not Connected. " + Environment.NewLine + "Please check server, port, SSL settings";
                }
            }
            catch (Exception ex)
            {
                _errMsg += "Exception:" + ex.GetType().Name + ": " + ex.Message + Environment.NewLine;
                mailStatus = ex.Message;
                _errorsFound = true;
            }
            client.Disconnect();
            return mailStatus;


        }



        public void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            string token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }

        public string SendMsg(string file, string to, string mySubject, string msg)
        {
            var myMsg = new MailMessage();
            var addresses = to.Split(';');
            MailAddressCollection ToList = new MailAddressCollection();
            if (addresses.Length > 1)
            {
                for (int i = 0; i < addresses.Length; i++)
                {
                    if (!string.IsNullOrEmpty(addresses[i]))
                    {
                        MailAddress mAddress = new MailAddress(addresses[i]);
                        ToList.Add(mAddress);
                        //  myMsg.To.Add(new MailAddress(addresses[i]));
                    }
                }

            }
            else
            {
                MailAddress mAddress = new MailAddress(to);
                ToList.Add(mAddress);
                //  myMsg.To.Add(new System.Net.Mail.MailAddress(to));
            }
            myMsg.To.Add(ToList.ToString());

            if (file != "")
            {
                try
                {
                    if (File.Exists(file))
                    {
                        myMsg.Attachments.Add(new System.Net.Mail.Attachment(file));

                    }
                }
                catch (Exception ex)
                {
                    SendMsg("", AlertsTo, "Error Sending Alert Email", "Error Message is: " + ex.Message + Environment.NewLine + "Original Message to send was: " + Environment.NewLine + msg);
                }
            }
            myMsg.From = new System.Net.Mail.MailAddress(MailServerSettings.Email, "CTC Adapter");
            myMsg.Subject = mySubject;
            myMsg.Body = msg;
            SmtpClient myMail = new SmtpClient
            {
                Host = MailServerSettings.Server,
                Port = Convert.ToInt16(MailServerSettings.Port),
                //myMail.EnableSsl = ssl;
                Credentials = new System.Net.NetworkCredential(MailServerSettings.UserName, MailServerSettings.Password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            myMail.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            //String userState = "Send from CTCNODE";
            try
            {
                myMail.Send(myMsg);
                myMsg.Dispose();
                myMail.Dispose();

                return "Message Sent OK";
            }
            catch (Exception ex)
            {
                _errMsg += "Exception:" + ex.GetType().Name + ": " + ex.Message + Environment.NewLine;
                myMsg.Dispose();
                myMail.Dispose();
                _errorsFound = true;
                return "Message failed to send. Error was: " + ex.Message;

            }
        }
        #endregion

        #region helpers
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                if (client.Connected)
                {
                    client.Disconnect();
                }
                client.Dispose();
            }

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }
        #endregion




        // Public implementation of Dispose pattern callable by consumers.


        // Protected implementation of Dispose pattern.







    }
}

