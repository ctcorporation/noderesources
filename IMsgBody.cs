﻿namespace NodeResources
{
    public interface IMsgBody
    {
        string MsgFrom { get; set; }
        string MsgStatus { get; set; }
        string MsgSubject { get; set; }
        string MsgText { get; set; }
        string MsgTo { get; set; }
    }
}