﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace NodeResources
{
    public static class ExcelToTable
    {
        public static DataTable ToTable(string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {

                SpreadsheetDocument xs = SpreadsheetDocument.Open(fs, false);
                WorkbookPart workbookPart = xs.WorkbookPart;
                IEnumerable<Sheet> sheets = xs.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)xs.WorkbookPart.GetPartById(relationshipId);
                Worksheet worksheet = worksheetPart.Worksheet;
                SheetData sheetData = worksheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();
                DataTable dt = new DataTable();
                foreach (Cell cell in rows.ElementAt(0))
                {
                    DataColumnCollection colColl = dt.Columns;
                    string colName = ExcelHelper.GetCellValue(xs, cell).Trim();
                    int i = 1;
                    while (colColl.Contains(colName))
                    {
                        colName = ExcelHelper.GetCellValue(xs, cell).Trim() + "-" + i;
                        i++;
                    }
                    dt.Columns.Add(colName);
                }
                int colcount = dt.Columns.Count;
                foreach (Row row in rows)
                {
                    try
                    {
                        
                        DataRow tempRow = dt.NewRow();
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)ExcelHelper.GetColumnIndexFromName(ExcelHelper.GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);

                            }
                            tempRow[columnIndex] = ExcelHelper.GetCellValue(xs, cell).Trim();
                            columnIndex++;
                            if (dt.Rows.Count == 82)
                            {
                                var cCount = tempRow.Table.Columns.Count;
                            }
                        }

                        dt.Rows.Add(tempRow);
                    }
                    catch (Exception)
                    {

                    }
                }
                fs.Close();

                dt.Rows.RemoveAt(0);
                return dt;


            }
        }
    }
}
