﻿namespace NodeResources
{
    public interface IConnectionManager
    {
        string ConnString { get; set; }

        string ServerName { get; set; }
        string DatabaseName { get; set; }
        string User { get; set; }
        string Password { get; set; }


        string BuildConnectionString(string server, string database, string user, string password);
        string BuildEntityConnectionString(string sqlString);
    }
}