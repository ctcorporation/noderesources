﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using System.Xml;
using System.Xml.Linq;


namespace NodeResources
{
    class NodeRes
    {
        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    bool emptyCol = true;
                    for (int i = 0; i < dr.ItemArray.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(dr[i].ToString()))
                        {
                            emptyCol = false;
                        }
                    }
                    if (!emptyCol)
                    {
                        dt.Rows.Add(dr);
                    }
                }
            }
            sr.Close();
            return dt;
        }

        public static DataTable ConvertCSVtoDataTable(string strFilePath, int headerFrom, int dataFrom)

        {
            StreamReader sr = new StreamReader(strFilePath);
            for (int i = 0; i < dataFrom; i++)
            {
                sr.ReadLine();















            }
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {

                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }


        public static string GetMessageNamespace(string messageFilePath)
        {
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    ReadToNextElement(reader);
                    return reader.NamespaceURI;
                }
            }
        }

        public static void ReadToNextElement(XmlReader reader)
        {
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    break;
                }
            }
        }

        public static string GetApplicationCode(string messageNamespace)
        {
            string appCode = string.Empty;
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    appCode = "UDM";
                    break;

                case "http://www.cargowise.com/Schemas/Native":
                    appCode = "NDM";
                    break;

                case "http://www.edi.com.au/EnterpriseService/":
                    appCode = "XMS";
                    break;

                default:
                    appCode = "";
                    break;
            }

            return appCode;


        }

        public static string GetXMLType(String messageFilePath)
        {
            string result = string.Empty;
            XDocument xDoc;
            using (var fileStream = GetFileAsStream(messageFilePath))
            {
                using (var reader = XmlReader.Create(fileStream))
                {
                    xDoc = XDocument.Load(reader);
                }
            }
            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            var xmlType = xDoc.Descendants().Where(n => n.Name == ns + "UniversalEvent").FirstOrDefault();
            if (xmlType != null)
            {

                result = xmlType.Name.LocalName;
            }
            return result;

        }

        public static string GetSchemaName(string messageNamespace)
        {
            switch (messageNamespace)
            {
                case "http://www.cargowise.com/Schemas/Native":
                case "http://www.cargowise.com/Schemas/Universal":
                case "http://www.cargowise.com/Schemas/Universal/2011/11":
                    return messageNamespace + "#UniversalInterchange";

                case "http://www.edi.com.au/EnterpriseService/":
                    return messageNamespace + "#XmlInterchange";

                default:
                    return messageNamespace;
            }
        }

        static Stream GetFileAsStream(string fileName)
        {
            return new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite);
        }





        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }
    }
}