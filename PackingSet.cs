﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeResources
{
    public class PackingSet : IPackingSet
    {
        private int pcs;
        private string packType;
        private decimal weight;
        private decimal volume;

        public int Pcs
        {
            get { return this.pcs; }
            set { pcs = value; }
        }

        public decimal Volume
        {
            get { return volume; }
            set { volume = value; }
        }

        public decimal Weight
        {
            get { return weight; }
            set { weight = value; }
        }
        public string PackType
        {
            get { return packType; }
            set { packType = value; }
        }
    }
}
