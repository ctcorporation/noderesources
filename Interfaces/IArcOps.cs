﻿namespace NodeResources.Interfaces
{
    public interface IArcOps
    {
        string ArchiveFile(string arcPath, string fileName);
        string ArchiveFile(string arcPath, string fileName, string logPath);
        void Dispose();
        string ExtractFile(string archive, string fileName, string Location);
    }
}